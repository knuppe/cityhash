CityHash.Net

## What is this?

> A fully managed port of the [City Hash] hash algorithm in C#.

> CityHash is a family of non-cryptographic hash functions, designed for fast hashing of strings.

> Google developed the algorithm in-house starting in 2010.
> The C++ source code for the reference implementation of the algorithm was released 
> in 2011 under an MIT license, with credit to Geoff Pike and Jyrki Alakuijala.

## Notes

> - This library has a few adaptations to perform better using .net technology.
> - The 300 tests of the original library are working perfectly. Whoot!


Please, consider donating as a thank you.

[![donate](https://raw.githubusercontent.com/knuppe/SharpNL/development/resources/donate.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=7SWNPAPJNSARC)

[![bean](https://raw.githubusercontent.com/knuppe/SharpNL/development/resources/bean.gif)](#)

[City Hash]:https://code.google.com/p/cityhash/
